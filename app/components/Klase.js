import React, { Component } from 'react'

export default class DefaultKlasa extends Component {
  constructor(props){
		super(props) // sluzi da pokupi instancu this-a
		console.log('this props: ',this.props)
		console.log('this: ',this)		
  }
	render() {
		return (
			<span>Default klasa {this.props.children} </span>   
		)
	}
}
