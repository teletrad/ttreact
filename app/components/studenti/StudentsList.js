import React, { Component } from 'react'
import studentsData from './studentsData'
import Attendee from './Attendee'
import { Button, ButtonToolbar } from 'react-bootstrap'

export default class Attendance extends Component {
  constructor(){
    super()
    this.state = {
      submitted: false,
      locked: false,
      present: 0,
      absent: 0,
      presentIds: [],
      absentIds: [],
      resetChild: false,
      unmarked: studentsData.length
    }
    this.initialState = this.state
  }

  presentCount = (id) => {
    // let arr = this.state.presentIds
    // arr.push(id)
    // arr.sort((a,b) => a > b)
    this.setState({
      // presentIds: arr,
      presentIds: [...this.state.presentIds, id].sort(),
      present: this.state.present + 1,
      unmarked: this.state.unmarked - 1
    })
  }

  absentCount = (id) => {    
    // let arr = this.state.absentIds
    // arr.push(id)
    this.setState({
      // absentIds: arr,
      absentIds: [...this.state.absentIds, id].sort(),
      absent: this.state.absent + 1,
      unmarked: this.state.unmarked - 1
    })
  }

  resetAttendee = (id) => { 
    if (this.state.presentIds.includes(id)) {
      this.setState({ 
        presentIds: this.state.presentIds.filter( index => index !== id ).sort(),       
        present: this.state.present - 1,
        unmarked: this.state.unmarked + 1
      })
    }
    else if (this.state.absentIds.includes(id)) {
      this.setState({ 
        absentIds: this.state.absentIds.filter( index => index !== id ).sort(),
        absent: this.state.absent - 1,
        unmarked: this.state.unmarked + 1
      })     
    }
  }

  submit = () => {
    if (this.state.unmarked !== 0) {
      console.warn('You have unmarked students. Please, mark them all and then submit.')
      return
    }
    const submittedIds = {
      present: this.state.presentIds,
      absent: this.state.absentIds
    }
    console.log(submittedIds)
    this.setState({
      submitted: true
    })
  }

  lock = () => {
    this.setState({
      locked: !this.state.locked
    })
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.resetChild){
      this.setState(this.initialState)
    }
  }
  

  reset = () => {
    this.setState({           
      resetChild: true
    })
  }

  attendeeList = () => {
    return studentsData.map((student) => {
      return(
        <Attendee
                key={student.id}
                firstName={student.firstName}
                lastName={student.lastName}
                onClickPresent={this.presentCount}
                onClickAbsent={this.absentCount}
                image={student.image}
                id={student.id}
                onClickReset={this.resetAttendee}
                resetChild={this.state.resetChild}
                lockChild={this.state.locked}
              />
      )
    })
  }

  render() {
    var totalStyle = {'margin': '10px 20px', 'width': '150px', 'float': 'right', 'backgroundColor': 'aquaMarine', 'padding': '0 5px', 'textAlign': 'center', 'boxSizing': 'borderBox'}
    var attendee = studentsData.map((student) => {
      return(
        <Attendee
          key={student.id}
          firstName={student.firstName}
          lastName={student.lastName}
          onClickPresent={this.presentCount}
          onClickAbsent={this.absentCount}
          image={student.image}
          id={student.id}
          onClickReset={this.resetAttendee}
          resetChild={this.state.resetChild}
          lockChild={this.state.locked}
        />
      )
    })
    
    return (
      <div>
        <div style={this.state.submitted ? {'display': 'none'} : {}}>
          <div style={{'float': 'left'}}> 
            {attendee}
            {/* this.attendeeList() */}
           {/* 
            <Attendee
              firstName={studentsData[0].firstName}
              lastName={studentsData[0].lastName}
              onClickPresent={this.presentCount}
              onClickAbsent={this.absentCount}
              image={studentsData[0].image}
              id={studentsData[0].id}
              onClickReset={this.resetAttendee}
              resetChild={this.state.resetChild}
              lockChild={this.state.locked}
            />
            <Attendee
              firstName={studentsData[1].firstName}
              lastName={studentsData[1].lastName}
              onClickPresent={this.presentCount}
              onClickAbsent={this.absentCount}
              image={studentsData[1].image}
              id={studentsData[1].id}
              onClickReset={this.resetAttendee}
              resetChild={this.state.resetChild}              
              lockChild={this.state.locked}
            />
            <Attendee
              firstName={studentsData[2].firstName}
              lastName={studentsData[2].lastName}
              onClickPresent={this.presentCount}
              onClickAbsent={this.absentCount}
              image={studentsData[2].image}
              id={studentsData[2].id}
              onClickReset={this.resetAttendee}
              resetChild={this.state.resetChild}
              lockChild={this.state.locked}
            />
            <Attendee
              firstName={studentsData[3].firstName}
              lastName={studentsData[3].lastName}
              onClickPresent={this.presentCount}
              onClickAbsent={this.absentCount}
              image={studentsData[3].image}
              id={studentsData[3].id}
              onClickReset={this.resetAttendee}
              resetChild={this.state.resetChild}
              lockChild={this.state.locked}
            />*/}
          </div>        
          <div style={totalStyle}>
            <h2>Total Attendances</h2>
            <p>Present: {this.state.present} {`[${this.state.presentIds}]`} </p>
            <p>Absent: {this.state.absent} {`[${this.state.absentIds}]`} </p>
            <p>Unmarked: {this.state.unmarked}</p>
             
          </div>
          <div style={{'clear': 'both', 'margin': '50px 0px 0px 10px'}}>
            <ButtonToolbar>
              <Button onClick={this.submit} bsSize="small" disabled={this.state.locked}>Submit</Button>
              <Button onClick={this.lock} bsSize="small">Lock</Button>
              <Button onClick={this.reset} bsSize="small" disabled={this.state.locked}>Reset</Button>
            </ButtonToolbar>
           
          </div>
        </div>
        <div style={this.state.submitted ? {} : {'display': 'none'}}> Thank you for your submission!
        </div>
      </div>
    );
  }
}
