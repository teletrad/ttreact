import React, { Component } from 'react'
import { Button, ButtonToolbar } from 'react-bootstrap'

export default class Attendee extends Component {
  constructor(props){
    super(props)
    this.state = {
      locked: false,
      markPresent: false,
      markLate: false,
      markAbsent: false,
      reset: false
    }    
    this.initialState = this.state
  }
  
  
  componentWillReceiveProps(nextProps) {
    if (this.state.locked !== nextProps.lockChild){
      if (nextProps.lockChild) {
        this.setState({
          locked: true,
          markAbsent: this.state.markAbsent,
          markPresent: this.state.markPresent,
          reset: this.state.reset,
          markLate: this.state.markLate

        })
      }
      else {
        this.setState({
          locked: false
        })
      }
    }
    if (nextProps.resetChild) {
      this.setState(this.initialState)
    }
  }  
  
  present = () => {
    if (!this.state.locked) {
    this.setState({
      // markPresent: true
      markAbsent: false,
      reset: true,
      markPresent: !this.state.markPresent
    }, this.props.onClickPresent(this.props.id))  // callback to parent component func
  }
  }

  absent = () => {
    if (!this.state.locked) {
      this.setState({
        // markAbsent: true
        markAbsent: !this.state.markAbsent,
        reset: true,
        markPresent: false,
      }, this.props.onClickAbsent(this.props.id))
    }
  }

  reset = () => {
    if (!this.state.locked) {
      this.setState({
        // markAbsent: true
        markAbsent: false,
        reset: false,
        markPresent: false,
      }, this.props.onClickReset(this.props.id))
    }
  }

  render() {
    const { firstName, lastName, onClickPresent, onClickAbsent, image, id, onClickReset} = this.props
    const name = `${firstName} ${lastName}`  // selektor mala funkcija koja izvlaci nesto iz stejta
    // const imagePath = image.replace(/\.\//ig, './app/')
    const imagePath = image.replace(/\.\//ig, '/app/components/studenti/')
    // const imagePath = require(`${image}`)
    const imageStyle = {'width': '100%', 'height': '100%'}
    const divStyle = {'width': '150px', 'margin': '10px', 'float': 'left', 'textAlign':'center', 'border': '2px solid white', 'backgroundColor': (this.state.markPresent ? '#ccffcc' : (this.state.markAbsent ? 'red' : 'transparent')) }
    const presentStyle = {}
    const absentStyle = {}

    return (
      <div style={divStyle}>
        <div><img src={require(`${image}`)} alt={`${firstName} ${lastName} - photo`} style={imageStyle} /></div>
        <div style={{'textAlign': 'center', 'width': '100%'}}>{name}</div>
        <div style={this.state.markAbsent || this.state.markPresent ? {'display': 'none'} : {} } >          
            <Button bsStyle="success" bsSize="small" onClick={this.present} style={{'width': '50%'}} disabled={this.props.lockChild}>
              <span>present</span>
            </Button>
            <Button bsStyle="danger" bsSize="small" onClick={this.absent} style={{'width': '50%'}} disabled={this.props.lockChild}>
              <span>absent</span>
            </Button>                
        </div>
        <div style={this.state.reset ? {} : {'display': 'none'}} >
         <Button onClick={this.reset} bsSize="small">X</Button>
        </div>
      </div>
    );
  }
}
