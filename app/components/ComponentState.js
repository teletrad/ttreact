import React, { Component } from 'react'
import ChildState from './ChildState'

export default class ComponentState extends Component {
  constructor(props){
    super(props)
    this.state = { count: 0 }
  }
  // state = {count: 0}  // isto kao u konstruktoru, samo bez this
  tick = () => {
    this.setState({
      count: this.state.count + 1
    })
  }

  render() {
    return (
      <div>
        {/*{<button onClick={this.tick}> Click count</button>}*/}
        <span>it is: {this.state.count}</span>
        <ChildState callbackTick={this.tick} />
      </div>
    );
  }
}