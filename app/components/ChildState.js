import React, { Component, PropTypes } from 'react'

export default class ChildState extends Component {
  
  render() {
    return (
      <button onClick={this.props.callbackTick}>Call parent thick()</button>
    );
  }
}

ChildState.PropTypes = {
  callbackTick: PropTypes.func.isRequired
}
