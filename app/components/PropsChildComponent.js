import React, { Component, PropTypes } from 'react'

// export default class ChildComponent extends Component {
//   state = {  }
//   render() {
//     const {name, lastName, height} = this.props
//     return (
//       <div>name: {name} {lastName}, height: {height}cm</div>
//     );
//   }
// }

export default function ChildComponent(props) {
    const {name, lastName, height} = props
    return (
      <div>name: {name} {lastName}, height: {height}cm</div>
    );
 
}

ChildComponent.propTypes = {
  name: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  height: PropTypes.number.isRequired
}

