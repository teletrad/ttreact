import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
require ('../styles/style.css')
require ('../styles/animation.css')
export default class ComponentWithStyle extends Component {
  
  render() {
    return (
      //<span style={{color:'red', fontSize: '29px'}}>text koji hocemo da <a className={!true ? 'link' : 'link2'}>stilizujemo</a></span>    
      <ReactCSSTransitionGroup
        component='div'
        transitionName='animation-slide'
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnterTimeout={500}
        transitionLeaveTimeout={300}
      > 
        <div key={1} style={{color:'red', fontSize: '22px'}}>Sadrzaj za animaciju</div>
      </ReactCSSTransitionGroup>
    );
  }
}