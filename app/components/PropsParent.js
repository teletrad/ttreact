import React, { Component } from 'react'
//import PropTypes from 'prop-type'
import ChildComponent from './PropsChildComponent'

export default class PropsParent extends Component {
  state = {  }
  render() {
    
    return (
      <ChildComponent 
        name='Arnold' 
        lastName='Schwartzenegger' 
        height={195} 
      />
    );
  }
}


