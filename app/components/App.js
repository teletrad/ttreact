import React, { Component } from 'react'
import DefaultKlasa from './Klase';
import ComponentWithStyle from './Stilizovana'
import PropsParent from './PropsParent'
import ComponentState from './ComponentState'
import Attendance from './studenti/StudentsList'

const poruka = 'Ovo je nova poruka'
const element = (<span> Neki text </span>)
const element1 = (<span>{ poruka }</span>)
const element2 = React.createElement(
	'h1', // type
	{className: 'greeting'}, // props
	'React.createElement - Hello, world!' // children
)

const element3 = (
	<div>
		{element2}
		<h1>Hello, world!</h1>
		<h2>It is {new Date().toLocaleTimeString()}.</h2>
	</div>
)

export default class App extends Component {
	render() {
		//return element3
		//return <div>{element}{element3}</div>
		//return <DefaultKlasa />
		//return <DefaultKlasa ime={'igor'}> childreeen {element3}	</DefaultKlasa>
		//return <ComponentWithStyle />
		//return <PropsParent />
		//return <ComponentState />
		return <Attendance />
	}
}