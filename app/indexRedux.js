import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import counter from './reduxPractice/reducer'
import Counter from './reduxPractice/Counter'

const store = createStore(counter)

const render = () => ReactDOM.render(
  <Counter 
    value = {store.getState()}
    dispatch = {store.dispatch}
  />,
  document.getElementById('root')
)
render()
store.subscribe(render)
