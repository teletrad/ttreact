import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
// import App from './components/App'
import Root from './router/root'
import {Router} from 'react-router-dom'
import {Provider} from 'react-redux'
import { createStore, compose } from 'redux'
import { install } from 'redux-loop'
import createBrowserHistory from 'history/createBrowserHistory'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import appReducer from './reduxPractice/reducer'
import Counter from './reduxPractice/CounterConnect'
import loopReducer from './reduxPractice/loopReducer'
import LoopCounter from './reduxPractice/LoopCounter'
import LoopExample from './reduxPractice/loopExample'



// kod za redux debug developer tool
const finalCreateStore = compose(
	install(),
	window.devToolsExtension ? window.devToolsExtension({
		deserializeState: state => state && new AppStateWithEffects(state.appState, [])
	}) : f => f
) (createStore)

// const store = finalCreateStore(appReducer)
const store = finalCreateStore(loopReducer)

const newHistory = createBrowserHistory()

// const store = createStore(counter)
//appContainer je za hot reload
const render = Component => {
	ReactDOM.render (		
		<AppContainer>
			<Provider store={store}>
				<Router history={newHistory}>
					<MuiThemeProvider>
						<Component />
					</MuiThemeProvider>
				</Router>
			</Provider>
		</AppContainer>,
		document.getElementById('root')
	)
}
// render(App)
//ReactDOM.render(<App/>, document.getElementById('root'))
// render(Counter)
//render(LoopCounter)
render(LoopExample)

if (module.hot) {
	// module.hot.accept('./components/App', () => { render(App) })
	module.hot.accept('./router/root', () => { render(Root) })
}

