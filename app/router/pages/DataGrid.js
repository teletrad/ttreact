import React, { Component } from 'react'
import {Tabs, Tab} from 'material-ui/Tabs'
import {getUsers} from './dynamicData/apiHelpers'
import ReactDataGrid from 'react-data-grid'

import Snackbar from 'material-ui/Snackbar';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
}

export default class DataGridComponent extends Component {
  constructor(props){
    super(props)
  }
  state = { 
    value: 'a',
    users: [],
    details: false,
    openSnack: false,
    loading: true,
    columns: [
      { key: 'id', name: 'ID', sortable: true}, 
      { key: 'uname', name: 'Name', sortable: true },
      { key: 'username', name: 'Username', sortable: true },
      { key: 'email', name: 'email', sortable: true },
      { key: 'phone', name: 'Phone', sortable: true },
      { key: 'website', name: 'Website', sortable: true}
    ],
    rows: []
  }
  // render() {
  //   return (
  //     <div>hell</div>
  //   );
  // }

  componentDidMount() {  
    return getUsers(this.callback) 
  }

  callback = (data) => {
    console.log(data)
    if (data.data.length) {
      // let row = this.state.users.map((item, index) => {
      let rows = data.data.map((item, index) => {
        return { 
          id: item.id, 
          uname: item.name,
          username: item.username,
          email: item.email,
          phone: item.phone,
          website: item.website
        }
      })
      this.setState({
        users: data.data, 
        loading: false, 
        openSnack: true, 
        rows
      }, () => {console.log(this.state.rows)})
    } else {
      this.setState({
        loading: false,
        openSnack: true
      })
    }
  }

  showDetails = () => {
    this.setState({details: !this.state.details})
  }

  handleChange = (value) => {
    this.setState({
      value: value,
    });
  };

  handleRequestCloseSnack = () => {
    this.setState({
      openSnack: false,
    });
  }

  // rowGetter = (i) => {
  //   return this.state.rows[i];
  // }

  rowGetter = rowNumber => this.state.rows[rowNumber]

  handleGridSort = (sortColumn, sortDirection) => {
    const comparer = (a, b) => {
      if (sortDirection === 'ASC') {
        return (a[sortColumn] > b[sortColumn]) ? 1 : -1;
      } else if (sortDirection === 'DESC') {
        return (a[sortColumn] < b[sortColumn]) ? 1 : -1;
      }
    };

    // const rows = sortDirection === 'NONE' ? this.state.originalRows.slice(0) : this.state.rows.sort(comparer);
    const rows = sortDirection === 'NONE' ? this.state.rows.slice(0) : this.state.rows.sort(comparer);

    this.setState({ rows });
  }

  render() {
    // const columns = [
    //   { key: 'id', name: 'ID' }, 
    //   { key: 'uname', name: 'Name' },
    //   { key: 'username', name: 'Username' },
    //   { key: 'email', name: 'email' },
    //   { key: 'phone', name: 'Phone' },
    //   { key: 'website', name: 'Website' }
    // ]
    // const rowGetter = rowNumber => rows[rowNumber]
    // let rows = this.state.users.map((item, index) => {return { 
    //   id: item.id, 
    //   uname: item.name,
    //   username: item.username,
    //   email: item.email,
    //   phone: item.phone,
    //   website: item.website
    // }})
    return (
      <Tabs
        value={this.state.value}
        onChange={this.handleChange}
      >
        <Tab label="Tab A" value="a">
          <div>
            <h2 style={styles.headline}>Controllable Tab A</h2>
            <p>
              Tabs are also controllable if you want to programmatically pass them their values.
              This allows for more functionality in Tabs such as not
              having any Tab selected or assigning them different values.
            </p>
            <div> 
        <h3> Dinamicki podaci </h3>
        {this.state.loading
        ? <span> ucitavanje u toku... </span>
        : this.state.users.length
            ? <div>
                  {/*this.state.users.map((item, index) => {
                    // kad imamo nesto sto se renderuje vise puta, moramo imati key atribut
                    return <div key={index} data-id={index} data-item={item}> {item.name}</div> 
                  })*/}
                  <div>
                    <ReactDataGrid
                      columns={this.state.columns}
                      onGridSort={this.handleGridSort}
                      rowGetter={this.rowGetter}
                      rowsCount={this.state.rows.length}
                      minHeight={500} />
                  </div>
                  <Snackbar
                    open={this.state.openSnack}
                    style={this.state.users.length ? {'backgoundColor':'green'} : {'backgoundColor':'red'}}
                    message={this.state.users.length ? "Everything loaded correctly!" : 'Error!'}
                    autoHideDuration={4000}
                    onRequestClose={this.handleRequestCloseSnack}
                  />
                </div>
            : <div style={{'border':'2px solid red', 'backgoundColor':'pink'}}>
              Nema podataka
              </div>          
        }
      </div>
          </div>
        </Tab>
        <Tab label="Tab B" value="b">
          <div>
            <h2 style={styles.headline}>Controllable Tab B</h2>
            <p>
              This is another example of a controllable tab. Remember, if you
              use controllable Tabs, you need to give all of your tabs values or else
              you wont be able to select them.
            </p>
          </div>
        </Tab>
      </Tabs>
    )
  }

}