import React, { Component } from 'react'
import axios from 'axios'

export function getUsers(callback) {
  return axios.get('https://jsonplaceholder.typicode.com/users', {method: 'GET'}).then((data) => {
        callback(data)
      })
}