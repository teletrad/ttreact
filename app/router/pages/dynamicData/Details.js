import React, { Component } from 'react'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table'
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()

export default class Details extends Component {
  constructor(props){
    super(props)
    //state
  }
  state = { 
    open: false,
    details: false 
  }

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  componentDidMount () {
    document.addEventListener('click', this.showDetails, false)
  }
  
  componentWillUnmount() {
    document.removeEventListener('click', this.showDetails, false);
  }

  showDetails = (e) => {
    //console.log(e.target.parentElement)
    //console.log(this.refs.refId)
    if (e.target.parentElement === this.refs.refId){
      this.setState({details: !this.state.details})
      console.log('@@@ test')
      // this.handleOpen()
    }
    else {
      this.setState({details: false})
      // this.handleClose()
    }
  }

  isSelected = () => {
    console.log('### in isselected')
  }

  handleCellClick = () => {
    console.log('### Cell is clicked...')
  }

  render() {
    var item = this.props.item
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        disabled={false}
        onTouchTap={this.handleClose}
      />
    ]

    return (
      <TableRow ref='refId' onTouchTap={this.handleOpen}> 
        <TableRowColumn>{item.name}</TableRowColumn>
        <TableRowColumn>{item.email}</TableRowColumn>
        {/*this.state.details && <Details show={this.state.details} item={item} />*/}
        {/*this.state.details && <td> &nbsp; &nbsp; {item.name}</td>*/}
        <TableRowColumn style={!this.state.details ? {'width':'0px'} : {display:'none','width':'0px'}}> 
          
          <Dialog
            title={`Details for ${item.name}`}
            actions={actions}
            modal={true}
            open={this.state.open}
          >
            <div>
              <div>ID: {item.id}</div>
              <div>Username: {item.username}</div>
              <div>Name: {item.name}</div>
              <div>Phone: {item.phone}</div>
              <div>Website: {item.website}</div>
              <div>Address: {`${item.address.city}, ${item.address.street} ${item.address.suite}`}</div>
              <div>Company: {item.company.name}</div>
            </div>
          </Dialog>
        </TableRowColumn>
      </TableRow>
                    
    )
  }
}
//<td><tr><td>item.address</td></tr></td>