import React, { Component } from 'react'
import axios from 'axios'
import {getUsers} from './apiHelpers'
import Details from './Details'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table'

export default class Dynamic extends Component {
  constructor(props) {
    super(props)
    this.state = {
      users: [],
      details: false,
      loading: true
    }
  }

  // state = {
  //     users: [],
  //     loading: true
  //   }

  callback = (data) => {
    console.log(data)
      data.data.length
            ? this.setState({users: data.data, loading: false})
            : this.setState({loading: false})
  }

  componentDidMount() {
    // return axios.get('https://jsonplaceholder.typicode.com/users',
    // {method: 'GET'})
    //   .then((data) => {
    //     data.data 
    //     ? this.setState({users: data.data, loading: false})
    //     : this.setState({loading: false})
    //   })
  
    return getUsers(this.callback) 
  }

  showDetails = () => {
    this.setState({details: !this.state.details})
  }
  
  handleRowSelection = (e) => {
    console.log('@@@ Selected row: ', e)
  }

  handleRowClick = () => {
    console.log('@@@@ Row clicked...')
  }

  handleCellClick = () => {
    console.log('@@@@ Cell clicked - Parent...')
  }

  render() {
    return (
      <div> 
        <h3> Dinamicki podaci </h3>
        {this.state.loading
        ? <span> ucitavanje u toku... </span>
        : this.state.users.length
            ? <Table style={{backgroundColor:'lightBlue'}} selectable={true}>
                <TableHeader displaySelectAll={false} adjustForCheckbox={false} >
                  <TableRow>
                    <TableHeaderColumn style={{color:'black', fontWeight:'bold', fontSize:'20px'}}>Name</TableHeaderColumn>
                    <TableHeaderColumn style={{color:'black', fontWeight:'bold', fontSize:'20px'}}>email</TableHeaderColumn>
                  </TableRow>
                </TableHeader>
                <TableBody>
                  {this.state.users.map((item, index) => {
                    // kad imamo nesto sto se renderuje vise puta, moramo imati key atribut
                    return <Details key={index} id={index} item={item} /> 
                  })}
                </TableBody>
              </Table>
            : <div>Nema podataka</div>          
        }
      </div>
    )
  }
}