import React, { Component } from 'react'

export default class Forms extends Component {
  state = { 
    //textInput: {name: '', valid: false}
    name:'',
    isGoing: false,
    isValid: false,
    valid: false,
    toSpan: false
  }

  handleChange = (event) => {
    console.log(event)
    const v = event.target.value
    const test = this.nameRegEx.test(v)
    this.setState({
      name: event.target.value,
      valid: test
    })
  }

  handleCheckChange = (event) => {
    console.log(event.target.checked)
    console.log(event.target.name)
    this.setState({[event.target.name]: event.target.checked})
  }

  nameRegEx = /^[a-zA-Z]+$/
  onInputBlur = (event) => {
    const v = event.target.value
    const test = this.nameRegEx.test(v)
    test 
    ?
      this.setState({
        valid: test,
        toSpan: true
      })
    :
      this.setState({
        valid: test
      })
  }

  transform = () => {
    this.setState({
      toSpan: false
    })
  }

  focus = () => {
    console.log(this.refs)
    this.refs.textarea.focus()
  }

  render() {
    return (
      <form action="" onSubmit={this.handleSubmit}>
        <h1>Forms</h1>
        <div>
          <label htmlFor="">
            Name:
            {
              this.state.toSpan 
              ?
                <span onClick={this.transform}>{this.state.name}</span>
              :
                <input 
                  style={this.state.valid ? {'border':'2px solid green'}: {'border':'2px solid red'}}
                  type="text"
                  value={this.state.name}
                  onChange={this.handleChange}
                  onBlur={this.onInputBlur}
                  ref='refname'
                />
            }
          </label>
        </div>
        <div>
          <label htmlFor="">
            Comment:
            <textarea
              value={this.state.comment}
              onChange={this.handleChangeComment}
              ref='textarea'
            />
          </label>
        </div>
        <div>
          <label htmlFor="">
            Pick your favourite fruit:
            <select 
              value={this.state.fruit} 
              onChange={this.handleChangeFruit}
            >
              <option value="1">Grapefruit</option>
              <option value="2">Lime</option>
              <option value="3">Coconut</option>
              <option value="4">Mango</option>
            </select>
          </label>
        </div>
        <div>
          <label htmlFor="">
            Is going:
            <input 
              type="checkbox" 
              name="isGoing"
              checked={this.state.isGoing}
              onChange={this.handleCheckChange} />
          </label>
        </div>
        <div>
          <label htmlFor="">
            Is valid:
            <input 
              type="checkbox" 
              name="isValid"
              checked={this.state.isValid}
              onChange={this.handleCheckChange} />
          </label>
        </div>
        <div>
          <input type="button"
            value="Focus text input"
            onClick={this.focus}
          />
        </div>
      </form>      
    )
  }
}