import React from 'react'
import { Route, Redirect, Switch, Link } from 'react-router-dom'
import { About } from './pages/About'
import { Contact } from './pages/Contact'
import { Attendance } from '../components/studenti/StudentsList'

const Links = () => {
  return (
    <nav>
      <Link to='/about'>About</Link> <br />
      <Link to='/contact'> -Contact</Link> <br />
      <Link to='/inbox'> -Poruke</Link>
    </nav>
  )
}

const notFound = () => <div><h3>404 Not Found</h3></div>

function SwitchRouter() {
  return (
    <div>
      <Links />
      <Switch>
        <Route path='/about' component={About} />
        <Route path='/contact' component={Contact} />
        <Route component={notFound} />
      </Switch>
    </div>
  )
}

export default SwitchRouter
