import React from 'react'
import { Route, Redirect, Switch, Link } from 'react-router-dom'

import { About } from './pages/About'
import { Contact } from './pages/Contact'
import { Inbox } from './pages/Inbox'

import Forms from './pages/Forms'

import Attendance from '../components/studenti/StudentsList'
// import Dynamic from './pages/dynamicData/index'
import Dynamic from '../router/pages/dynamicData/index'
import DataGridComponent from './pages/DataGrid'

require ('../styles/style.css')

const notFound = () => <div><h3>404 Not Found</h3></div>
const oldC = () => <div><h3>Old</h3></div>
const newC = () => <div><h3>New</h3></div>

const Links = () => {
  return (
    <nav>
      <Link to='/about'>About</Link> <br />
      <Link to='/contact'> -Contact</Link> <br />
      <Link to='/inbox'> -Poruke</Link> <br />
      <Link to='/forms'>Forms</Link> <br />
      <Link to='/new'> -new</Link> <br />
      <Link to='/old'> -old</Link> <br />
      <Link to='/studenti'> -studenti</Link> <br/>
      <Link to='/dynamic'> -dynamic</Link> <br />
      <Link to='/dg'> -Data Grid</Link>

    </nav>
  )
}

function RedirectRouter() {
  return (
    <div>
      <Links />
      <Switch>
        <Route path='/about' component={About} />
        <Route path='/contact' component={Contact} />
        <Route path='/inbox' component={Inbox} />
        <Route path='/new' component={newC} />
        <Route path='/forms' component={Forms} />
        <Route path='/old' render={() => (
          <Redirect to='/new' />
        )} />
        <Route path='/studenti' component={Attendance} />
        <Route path='/dg' component={DataGridComponent} />
        <Route path='/dynamic' component={Dynamic} /> 
      </Switch>
    </div>
  )
}

export default RedirectRouter
