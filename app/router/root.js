import React, { Component } from 'react'
import { Route, Link } from 'react-router-dom'
import {About} from './pages/About'
import {Contact} from './pages/Contact'
import {Inbox} from './pages/Inbox'
import SwitchRouter from './switchRouter'
import RedirectRouter from './redirectRouter'

// const App = (p) => {return(<div><p>Zaglavlje</p>{p.children}</div>)}
class App extends Component {
  constructor(p){
    super(p)
    console.log('*** Inside constructor: ', p)
  }


  componentWillMount() {
    console.log('*** Inside componentWillMount ')
  }

  componentDidMount() {
    console.log('*** Inside componentDidMount ')
  }

  render() {
    console.log('*** Inside render ')
    return (
      <div>
        <p>Zaglavlje</p>
        <Links />
        {this.props.children}
      </div>
    )
  }
}

// const Home = () => <div><h2>Pocetna</h2></div>

class Home extends Component {  
  state = {isTrue: false} // automatski mapira na this.state inicijalnu vrednost
  update = () => this.setState({ isTrue: !this.state.isTrue })
  render() {
    return (
      <div>
        <h2>Pocetna</h2>
        <button onClick={this.update}>Update</button>
        <Nested isOK={this.state.isTrue} />
      </div>
    )
  }
}

class Nested extends Component {

  state = {ime: 'nikola'}

  componentWillReceiveProps(nextProps) {
    console.log('@@@ Inside componentWillReceiveProps ', nextProps)
  }  

  shouldComponentUpdate(nextProps) {
    console.log('@@@ Inside shouldComponentUpdate: ', nextProps)
    return true
  }
  
  componentWillUpdate(nextProps, nextState) {
    console.log('@@@ Inside componentWillUpdate: ', nextProps, nextState)
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('@@@ Inside componentDidUpdate: ', prevProps, prevState)
  }

  componentWillUnmount() {
    console.log('@@@ Inside componentWillUnmount: ')
  }

  render() {
    console.log('@@@ Inside render inside nested: ')
    return (
      <div>
        <h3>Nested</h3>
      </div>
    )
  }
}

const Links = () => {
  return (
    <nav>
      <Link to='/about'>About</Link> <br />
      <Link to='/contact'>Contact</Link> <br />
      <Link to='/inbox'>Inbox</Link>
    </nav>
  )
}

// function Root() {
//   return (
//     <App>
//       {/*<Links />*/}
//       <Route exact path={'/'} component={Home} />
//       <Route path={'/contact'} component={Contact} />
//       <Route path={'/about'} component={About} />
//       <Route path={'/inbox'} component={Inbox} />
//     </App>
//   )
// }

function Root() {
  // return <SwitchRouter />
  return <RedirectRouter />
}

export default Root
