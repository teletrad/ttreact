import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as actions from './actions'

export default class Counter extends Component {
  state = {  }

  onIncrement = () => {
    this.props.dispatch(actions.INCREMENT_ACTION_CREATOR())
  }

  onDecrement = () => {
    this.props.dispatch(actions.DECREMENT_ACTION_CREATOR())
  }

  handleInput = (e) => {
    this.props.dispatch(actions.SET_NAME_ACTION(e.target.value))
  }

  render() {
    const { value, dispatch, handleInput } = this.props
    // function onIncrement  (){
    //   this.props.dispatch({type: 'INCREMENT'})
    // }
    
    // function onDecrement  (){
    //   this.props.dispatch({type: 'DECREMENT'})
    // }
    return (
      <div>
        Clicked: {value.number} <br />
        <button onClick={this.onIncrement}>+</button>
        <button onClick={this.onDecrement}>-</button> <br />
        Name: {value.name} <br />
        <input onChange={this.handleInput} type="text"/>
      </div>
    );
  }
}

Counter.PropTypes = {
  value: PropTypes.number,
  // onIncrement: PropTypes.function,
  // onDecrement: PropTypes.function
}