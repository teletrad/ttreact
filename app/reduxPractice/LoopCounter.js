import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import * as actions from './actions'

class Counter extends Component {
  state = {  }
  
  render() {    
        
    return (
      <div>
        hello
      </div>
    );
  }
}


export default connect((state) => ({
  // mapStateToProps
  number: state.number
}), {
  // mapDispatchToProps
  increment: actions.INCREMENT_ACTION_CREATOR,
  decrement: actions.DECREMENT_ACTION_CREATOR,
  setName: actions.SET_NAME_ACTION_CREATOR,
  setCheckbox: actions.SET_CHECKBOX_ACTION_CREATOR,
  setLastName: actions.SET_LAST_NAME_ACTION_CREATOR
})(Counter)
