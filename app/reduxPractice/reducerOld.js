import { combineReducers } from 'redux'

const initState = {
        number: 0,
        name: '',
        checkbox: false
}

const initialDummyState = {
	dummyState: true,
	lastName: '',
	setAddress: ''
}
    // import fake reduce from fakerducrer

// EXPORT DEFAULT COMBINE REDUCERs(fakereducer, myreducer)

// myreducer
export const formReducer = (state = initState, action) => {
	switch (action.type) {
		// TIPOVI AKCIJA VELIKA SLOVA SA UNDERSCORE-om
		case 'INCREMENT':
			return {...state, number: state.number + 1 }
		case 'DECREMENT':
			return {...state, number: state.number - 1 }
		case 'SET_NAME':
			return ({...state, name: action.payload })
		case 'SET_CHECKBOX':
			return ({...state, checkbox: action.payload })
		default:
			return state
	}
}

export const fakeReducer = (state = initialDummyState, action) => {
	switch (action.type) {
		case 'CHANGE_DUMMY_STATE':
			return {...state, dummyState: !state.dummyState}
		case 'SET_ADDRESS':
			return {...state, setAddress: action.payload}
		case 'SET_LAST_NAME':
			return {...state, lastName: action.payload}
		default:
			return state
	}
}

const appReducer = combineReducers({ form: formReducer, fake: fakeReducer })

export default appReducer
