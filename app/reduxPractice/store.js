import React, { Component } from 'react'
import { createStore } from 'redux'
import counter from './reduxPractice/reducer'

const store = createStore(counter)

export default store