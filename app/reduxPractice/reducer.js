import { combineReducers } from 'redux'
import formReducer from './formReducer'
import fakeReducer from './fakeReducer'

const appReducer = combineReducers({ formReducer, fakeReducer })

export default appReducer