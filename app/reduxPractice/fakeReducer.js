
const initialDummyState = {
	dummyState: true,
	lastName: '',
	setAddress: ''
}

export default (state = initialDummyState, action) => {
	switch (action.type) {
		case 'CHANGE_DUMMY_STATE':
			return {...state, dummyState: !state.dummyState}
		case 'SET_ADDRESS':
			return {...state, setAddress: action.payload}
		case 'SET_LAST_NAME':
			return {...state, lastName: action.payload}
		default:
			return state
	}
}
