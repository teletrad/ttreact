import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import * as actions from './actions'

class Counter extends Component {
  state = {  }
  setName1 = (e) => {this.props.setName(e.target.value)}
  setLastName = (e) => {this.props.setLastName(e.target.value)}
  setCheckbox = (e) => {this.props.setCheckbox(e.target.checked)}

  render() {    
    const { number, name, increment, decrement, checkbox, lastName, fullName } = this.props
    
    return (
      <div>
        <p>{name}</p>
        Clicked: {number} <br />
        <button onClick={increment}>+</button>
        <button onClick={decrement}>-</button> <br />
        Name: {name} <br />
        Last Name: {lastName} <br />
        Full Name: {fullName} <br />
        <input onChange={this.setName1} type="text" disabled={checkbox} />
        <input onChange={this.setLastName} type="text" disabled={checkbox} />
        <input type="checkbox" onChange={this.setCheckbox} />
      </div>
    );
  }
}

Counter.PropTypes = {
  number: PropTypes.number,
  name: PropTypes.string,
  increment: PropTypes.func,
  decrement: PropTypes.func,
  setName: PropTypes.func
}

const getFullName = (ime, prezime) => {
  return ime + " " + prezime
}

export default connect((state) => ({
  // mapStateToProps
  number: state.formReducer.number,
  name: state.formReducer.name,
  checkbox: state.formReducer.checkbox,
  lastName: state.fakeReducer.lastName,
  fullName: getFullName(state.formReducer.name,state.fakeReducer.lastName)
}), {
  // mapDispatchToProps
  increment: actions.INCREMENT_ACTION_CREATOR,
  decrement: actions.DECREMENT_ACTION_CREATOR,
  setName: actions.SET_NAME_ACTION_CREATOR,
  setCheckbox: actions.SET_CHECKBOX_ACTION_CREATOR,
  setLastName: actions.SET_LAST_NAME_ACTION_CREATOR
})(Counter)
