
const initState = {
        number: 0,
        name: '',
        checkbox: false
}

export default (state = initState, action) => {
	switch (action.type) {
		// TIPOVI AKCIJA VELIKA SLOVA SA UNDERSCORE-om
		case 'INCREMENT':
			return {...state, number: state.number + 1 }
		case 'DECREMENT':
			return {...state, number: state.number - 1 }
		case 'SET_NAME':
			return ({...state, name: action.payload })
		case 'SET_CHECKBOX':
			return ({...state, checkbox: action.payload })
		default:
			return state
	}
}

