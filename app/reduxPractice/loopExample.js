import React, { Component } from 'react'
import {connect} from 'react-redux'
import * as actions from './actions'
import {getMultipleAsyncCalls} from './serviceHelper'

class Counterz extends Component {
  componentDidMount() {
    this.props.getUserList()
  }

  render(){
    const {increment, decrement, getUserList} = this.props
    return (
      <div>
        <button onClick={increment}>+</button>
        <button onClick={decrement}>-</button>
        <button onClick={getMultipleAsyncCalls}>ALL</button>
        <br />
        {this.props.number}
        <br />
        {
          this.props.loading
            ? <div> Loading </div>
            : console.log(this.props.userList[0])
            //: <div>{this.props.userList["0"]["address"]}</div>
        }
      </div>
    )
  }
}

export default connect((state) => ({
  number: state.number,
  loading: state.loading,
  userList: state.userList
}),{
  increment: actions.INCREMENT_ACTION_CREATOR,
  decrement: actions.DECREMENT_ACTION_CREATOR,
  getUserList: actions.getUserList
})(Counterz)
