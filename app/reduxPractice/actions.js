import { INCREMENT, DECREMENT, SET_NAME, SET_CHECKBOX, CHANGE_DUMMY_STATE, SET_ADDRESS } from './constants'

export const INCREMENT_ACTION_CREATOR = () => ({ 
  type: 'INCREMENT'
  //payload: v
})
// export const DECREMENT_ACTION = { type: 'DECREMENT'}
export const DECREMENT_ACTION_CREATOR = () => ({ 
  type: 'DECREMENT'
  //payload: v
})
export const SET_NAME_ACTION_CREATOR = (v) => ({ 
  type: 'SET_NAME',
  payload: v
})

export const SET_LAST_NAME_ACTION_CREATOR = (v) => ({ 
  type: 'SET_LAST_NAME',
  payload: v
})

export const SET_CHECKBOX_ACTION_CREATOR = (v) => ({ 
  type: 'SET_CHECKBOX',
  payload: v
})

export const CHANGE_DUMMY_STATE_ACTION_CREATOR = () => ({ 
  type: 'CHANGE_DUMMY_STATE'
})

export const SET_ADDRESS_ACTION_CREATOR = (value) => ({ 
  type: 'SET_ADDRESS',
  payload: value
})

// akcije za api poziv
export const getUserList = () => ({
  type: 'GET_USER_LIST'
})

export const getUserListSuccess = (v) => ({
  type: 'GET_USER_LIST_SUCCESS',
  payload: v
})

export const getUserPostsAndComments = (v1, v2) => ({
  type: 'GET_USER_POSTS_AND_COMMENTS_SUCCESS',
  posts: v1,
  comments: v2
})

export const getUserListFail = (e) => ({
  type: 'GET_USER_LIST_FAIL',
  payload: e
})

// ACTION CREATORS FOR CONNECT