import axios from 'axios'
import * as actions from './actions'

export const getUserList = () => {
  return axios.get('https://jsonplaceholder.typicode.com/users', {method: 'GET'})
    .then(data => actions.getUserListSuccess(data.data)) // po REST-u v2
    .catch(error => actions.getUserListFail(error))
}

var root = 'https://jsonplaceholder.typicode.com'

function getUserPosts() {
  return axios.get(`${root}/posts`, {method: 'GET'});
}

function getUserComments() {
  return axios.get(`${root}/comments`, {method: 'GET'});
}

// ubaci u reducer !!! 
export const getMultipleAsyncCalls = () => {
  return axios.all([getUserPosts(), getUserComments()])
    .then(axios.spread(function (posts, comments) {
    // Both requests are now complete
    console.log('posts ', posts.data)  
    console.log('comments ', comments.data)
    actions.getUserPostsAndComments(posts.data, comments.data)
  }))
  .catch(error => actions.getUserListFail(error))
}