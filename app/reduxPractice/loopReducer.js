
import { loop, Effects } from 'redux-loop'
import { getUserList } from './serviceHelper'

const initState = {
  number: 0,
  loading: false,
  userList: [],
  posts: [],
  comments: []
}

export default (state = initState, action) =>  {
  switch (action.type) {
    case 'GET_USER_LIST':
      return loop({
        ...state,
        loading: true
      },
      Effects.promise(getUserList))
    case 'GET_USER_LIST_SUCCESS':
      return {
        ...state,
        userList: action.payload,
        loading: false
      }
      case 'GET_USER_POSTS_AND_COMMENTS_SUCCESS':
      return {
        ...state,
        posts: action.posts,
        comments: action.comments,
        loading: false
      }
    default:
      return state
  }
};

