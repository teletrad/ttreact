
function foo(x,y) {
  return x + y;
}

var foo = (x,y) => x + y;

var func = x => {
  foo();
  return x * x; // must have return
}

var bar = e => {
  var b = 5;
  e * e
  console.log('bar: ', this)
}
bar();
//console.log(bar(3));

// var obj = {
var o = {
  x: 1,
  obj: {
    i: 10,
    b: () => console.log(this.i, this),
    c: function(){
      console.log(this.i, this);
    }
  }
}

o.obj.b();
o.obj.c();

