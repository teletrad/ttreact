
// hoisting
(function varTest() {
  var x = 1;
  if (true) {
    var x = 2;
    console.log(x);
  }
  console.log(x);
})();

(function varTest1() {
  let x = 1;
  if (true) {
    let x = 2;
    console.log(x);
  }
  console.log(x);
})();

(function varTest4(){
  var a = 1;
  var b = 2;

  if (a === 1) {
    var a = 11; // scope is global
    let b = 22; // scope is inside th if-block

    console.log(a);  // 11
    console.log(b);  // 22
  }
console.log(a); // 11
console.log(b);  // 2

})();
/*
(function do-something(){
  console.log(bar) // reference error
  let bar = 3;
  // let bar;  // no error
})();*/

(function varTest2(){
  console.log(foo=3, foo);
  var foo;
})();