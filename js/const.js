/*
const MY_FAV = 7;  // kod const - a uvek i deklarisemo i dodeljujemo vrednost

MY_FAV = 20;
console.log('my favourite number is: ' + MY_FAV);

//var MY_FAV = 20;

//let MY_FAV = 20;

if (MY_FAV === 7) {
  const MY_FAV = 20;  // isto ima block scope kao i let
  console.log('my favourite number is ' + MY_FAV);
  var MY_FAV = 20;
}
*/
const MY_OBJECT = {'key': 'value'};
//MY_OBJECT = {'OTHER_KEY': 'value'};
MY_OBJECT.key = 'otherValue';

const MY_ARRAY = [];
MY_ARRAY.push('A');
MY_ARRAY = ['B']; // error