
var a = [2,3,4];
var b = [1, ...a, 5];
var c = [1, a, 5];

console.log(b, c);

var str = "Hello World!";
var str1 = [...str];
console.log(str1);
console.log(str[3])

var obj = {x:1,y:4};
console.log(...obj);