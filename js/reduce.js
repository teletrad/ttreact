/*
let temp = [1,2,3,1,4,3,6,7];

let newTemp = temp.reduce( (prev, curr, index) => {
                // da broji koliko cega ima 
                return prev[curr] = prev[curr] ? prev[curr] + 1 : 0
                // prev[curr] = prev[curr] ? prev[curr] + 1 : 0;
                //   return prev[curr];
              }, {}) // previous current - dokle smo stigli u iteraciji, currentIndex, niz

*/
const temp = [1,1,2,3,2,4,1,6,1,7,3,6,1,3]

const counted = temp.reduce((prev, curr, index) => {
  //prev[next] = (prev[curr] || 0) + 1
  const exists = !!prev[curr]
  const newObj = {
    occurencies: exists ? prev[curr].occurencies + 1 : 1,
    // [index].unshift.apply(index, prev[curr].indexes)
    indexes: exists ? [...prev[curr].indexes, index] : [index]
  }
  
  prev[curr] = newObj
  return prev
}, {})

console.log(counted)

const reverted = Object.keys(counted).reduce((prev, curr) => {
  counted[curr].indexes.map((val) => prev[val] = Number(curr))
  return prev
}, [])

console.log(reverted)
			  
			  